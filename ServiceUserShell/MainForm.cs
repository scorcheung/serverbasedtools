﻿using System;
using System.Windows.Forms;
using AppShellSpace;
using WorkerSchedulerService;

namespace ServiceUserShell
{
    public partial class ServiceUserShell : Form
    {
        private WorkerSchedulerServiceCore WorkerSchedulerServiceCore;

        public ServiceUserShell()
        {
            InitializeComponent();
            WorkingPool.ServiceEntered = true;
        }

        private void ServiceUserShell_Load(object sender, EventArgs e)
        {
            this.WorkerSchedulerServiceCore = new WorkerSchedulerServiceCore();
        }

        private void btn_Start_Click(object sender, EventArgs e)
        {
            Common.Run((state) =>
            {
                this.WorkerSchedulerServiceCore.OnStopping = false;
                this.WorkerSchedulerServiceCore.OnStart();
                this.WorkerSchedulerServiceCore.DoWorkByService();
            });
        }

        private void btn_Stop_Click(object sender, EventArgs e)
        {
            Common.Run((state) =>
            {
                this.WorkerSchedulerServiceCore.OnStopping = true;
                this.WorkerSchedulerServiceCore.OnStop();
            });
        }

        private void btn_Pause_Click(object sender, EventArgs e)
        {
            Common.Run((state) =>
            {
                this.WorkerSchedulerServiceCore.OnPausing = true;
                this.WorkerSchedulerServiceCore.OnPause();
            });
        }

        private void btn_Resume_Click(object sender, EventArgs e)
        {
            Common.Run((state) =>
            {
                this.WorkerSchedulerServiceCore.OnPausing = false;
                this.WorkerSchedulerServiceCore.OnContinue();
                this.WorkerSchedulerServiceCore.DoWorkByService();
            });
        }

    }

}
