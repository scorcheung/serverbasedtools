using System;
using WorkerSchedulerService.Database.Tables;
using WorkerSchedulerService.Database.Table.Model.Interfaces;

namespace WorkerSchedulerService.Database.Table.Model.Interfaces
{
	internal partial interface IScheduleSync
	{
		/// <summary>
		/// 
		/// </summary>
		Int32 ID
		{ get; set; }


		/// <summary>
		/// 前一个计划ID
		/// </summary>
		Int32 PreviousPlanID
		{ get; set; }


		/// <summary>
		/// 当前计划ID
		/// </summary>
		Int32 OrderPlanID
		{ get; set; }


		/// <summary>
		/// 
		/// </summary>
		Int32 ParentID
		{ get; set; }


		/// <summary>
		/// 执行标记：0未执行，1已执行
		/// </summary>
		Int32 ExecutionTag
		{ get; set; }


	}
}