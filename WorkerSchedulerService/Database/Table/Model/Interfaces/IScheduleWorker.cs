using System;
using WorkerSchedulerService.Database.Tables;
using WorkerSchedulerService.Database.Table.Model.Interfaces;

namespace WorkerSchedulerService.Database.Table.Model.Interfaces
{
	internal partial interface IScheduleWorker
	{
		/// <summary>
		/// 
		/// </summary>
		Int32 ID
		{ get; set; }


		/// <summary>
		/// 工作名称
		/// </summary>
		String Name
		{ get; set; }


		/// <summary>
		/// 接口引用
		/// </summary>
		String InterfaceLink
		{ get; set; }


		/// <summary>
		/// 引用请求超时时长
		/// </summary>
		Int32 LinkTimeout
		{ get; set; }


		/// <summary>
		/// 接口参数
		/// </summary>
		String InterfaceArgs
		{ get; set; }


		/// <summary>
		/// 是否加载：1已加载，0未加载
		/// </summary>
		Int32 Loaded
		{ get; set; }


		/// <summary>
		/// 状态：0正常；1删除
		/// </summary>
		Int32 Status
		{ get; set; }


	}
}