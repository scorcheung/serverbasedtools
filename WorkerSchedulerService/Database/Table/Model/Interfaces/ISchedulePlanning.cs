using System;
using WorkerSchedulerService.Database.Tables;
using WorkerSchedulerService.Database.Table.Model.Interfaces;

namespace WorkerSchedulerService.Database.Table.Model.Interfaces
{
	internal partial interface ISchedulePlanning
	{
		/// <summary>
		/// 
		/// </summary>
		Int32 ID
		{ get; set; }


		/// <summary>
		/// 工作表ID
		/// </summary>
		Int32 WorkerID
		{ get; set; }


		/// <summary>
		/// 计划名称
		/// </summary>
		String Name
		{ get; set; }


		/// <summary>
		/// 计划启动时间
		/// </summary>
		DateTime BeginTime
		{ get; set; }


		/// <summary>
		/// 开始工作时间
		/// </summary>
		DateTime JobStartTime
		{ get; set; }


		/// <summary>
		/// 执行每次的时长（以毫秒为单位）
		/// </summary>
		Int32 ETA
		{ get; set; }


		/// <summary>
		/// 结束工作时间
		/// </summary>
		DateTime JobStopTime
		{ get; set; }


		/// <summary>
		/// 类别：0 Inactive（不活动的），1 固定时间，2 时间段 3 混合（时间段内的固定时间）
		/// </summary>
		Int32 PlanType
		{ get; set; }


		/// <summary>
		/// 是否加载：1已加载，0未加载
		/// </summary>
		Int32 Loaded
		{ get; set; }


		/// <summary>
		/// 最后完成时间
		/// </summary>
		DateTime LastFinishedTime
		{ get; set; }


		/// <summary>
		/// 状态：0正常；1删除；2修改
		/// </summary>
		Int32 Status
		{ get; set; }


	}
}