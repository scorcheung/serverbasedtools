using System;
using System.ComponentModel;
using MathBasic.Data;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.Info;
using MathBasic.Managements;

namespace WorkerSchedulerService.Database.Table.Model
{
    /// <summary>
    /// 此类表示数据库中表映射的基架构。
    /// </summary>
    internal abstract partial class TableSchemeBase : TableBase
    {

        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        protected TableSchemeBase()
        {
            base.DbServerType = Common.DbServerType;
            base.DbConnectionSelection = Common.DbConnectionString;
        }

        /// <summary>
        /// 获取表/视图的名称。
        /// </summary>
        internal String GetTableName()
        {
            return base.TableName;
        }

        /// <summary>
        /// 设置某个字段的值。
        /// </summary>
        /// <param name="fieldName">字段名称。</param>
        /// <param name="data">字段数据。</param>
        internal Boolean SetFieldValue(String fieldName, Object data)
        {
                DbElement field = base.GetDbElement(fieldName);
                if (field != null) {
                    this.SetDbElementData(field, (data != null ? Convert.ChangeType(data, field.DataType) : null));
                    return true;
                }
                else {
                    return false;
                }
        }

        /// <summary>
        /// 获取当前数据库的句柄。
        /// </summary>
        internal DbDatabase GetDbHandler()
        {
            return base.DbHandler;
        }

        /// <summary>
        /// 获取当前所使用的服务器类型。
        /// </summary>
        internal DbServerType GetServerType()
        {
            return base.DbServerType;
        }

        /// <summary>
        /// 验证某些字段是否可被表基础数据支持。
        /// </summary>
        /// <param name="fieldsToValidate">要验证的若干字段。</param>
        internal void ValidateFields(String[] fieldsToValidate)
        {
            this.ValidateWithGetFields(fieldsToValidate, null);
        }

        /// <summary>
        /// 验证某些字段是否可被表基础数据支持。
        /// </summary>
        /// <param name="fieldsToValidate">要验证的若干字段。</param>
        /// <param name="fieldFound">作用于当发现某个字段映射时所采取的操作；此时字段并未经过验证。</param>
        internal Int32 ValidateWithGetFields(String[] fieldsToValidate, MemberFoundAction<DbField> fieldFound)
        {
            return base.ValidateWithGetFields(fieldsToValidate, FieldTypes.None, (ArrayInfo.IsNullOrEmpty(fieldsToValidate) ? ValidOptions.IgnoreFixedPrimaryChecking : ValidOptions.GetByValidated), fieldFound, true);
        }

    }

    /// <summary>
    /// 此类表示数据库中表映射的最终基架构。
    /// </summary>
    internal abstract partial class TableScheme : TableSchemeBase
    {
        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        protected TableScheme() { }

    }

}

namespace WorkerSchedulerService.Database.Table.Model.Interfaces { }
