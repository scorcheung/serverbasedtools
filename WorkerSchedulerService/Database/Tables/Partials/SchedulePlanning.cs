﻿using System;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;

namespace WorkerSchedulerService.Database.Tables
{
    partial class SchedulePlanning
    {
        [DbFieldDataInfo(FieldTypes.None)]
        public SchedulerPlan RealPlan
        {
            get
            {
                switch ((SchedulerPlan)this.PlanType) {
                    case SchedulerPlan.Fixed:
                    case SchedulerPlan.TicksOnFixed:
                        if ((this.JobStopTime - this.JobStartTime).TotalMilliseconds == this.ETA) {
                            return SchedulerPlan.Fixed;
                        }
                        else {
                            return SchedulerPlan.TicksOnFixed;
                        }
                    case SchedulerPlan.Ticks:
                        return SchedulerPlan.Ticks;
                    default:
                        return SchedulerPlan.Inactive;
                }
            }
            set
            {
                this.PlanType = (Int32)value;
            }
        }

    }
}
