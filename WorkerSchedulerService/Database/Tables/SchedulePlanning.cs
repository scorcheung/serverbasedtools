using System;
using System.Data;
using System.Collections.Generic;
using MathBasic;
using MathBasic.Collections;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.Adapter;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using WorkerSchedulerService.Database;
using WorkerSchedulerService.Database.Tables;
using WorkerSchedulerService.Database.Table.Model;
using WorkerSchedulerService.Database.Table.Model.Interfaces;
using WorkerSchedulerService.Database.View.Model;
using WorkerSchedulerService.Database.View.Model.Interfaces;

namespace WorkerSchedulerService.Database.Tables
{
	internal sealed partial class SchedulePlanning : TableScheme, ISchedulePlanning
	{
		#region 实例自身相关的操作

        static SchedulePlanning()
        {
            SchedulePlanning scheme = SchedulePlanning.New;
            SchedulePlanning.m_ClassTableName = scheme.TableName;
            SchedulePlanning.m_PrimaryKeyName = Helpers.GetSpecificPrimaryKey(scheme.GetPrimaryFields());
        }

        private SchedulePlanning()
        {
        }

        /// <summary>
        /// 生成此类的新实例。
        /// </summary>
        public static SchedulePlanning New
        {
            get
            {
                return new SchedulePlanning();
            }
        }

		#endregion

		#region 获得表映射类的相关信息

		private static String m_PrimaryKeyName;
		/// <summary>
		/// 获取此类所映射的表的某个主键名称。
		/// </summary>
		public static String GetPrimaryKeyName()
		{
			return SchedulePlanning.m_PrimaryKeyName;
		}

		private static String m_ClassTableName;
		/// <summary>
		/// 获取此类所映射的表的名称。
		/// </summary>
		public static String GetClassTableName()
		{
			return SchedulePlanning.m_ClassTableName;
		}

		#endregion

		#region 已映射到表的字段属性

		/// <summary>
		/// 数据库字段 ID（System.Int32） 的字符串表示形式。源的注释为：
		/// </summary>
		public const String F_ID = "ID";
		/// <summary>
		/// 
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Primary)]
		public Int32 ID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 ISchedulePlanning.ID
		{ get{ return this.ID; } set{ this.ID = value; } }


		/// <summary>
		/// 数据库字段 WorkerID（System.Int32） 的字符串表示形式。源的注释为：工作表ID
		/// </summary>
		public const String F_WorkerID = "WorkerID";
		/// <summary>
		/// 工作表ID
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 WorkerID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 ISchedulePlanning.WorkerID
		{ get{ return this.WorkerID; } set{ this.WorkerID = value; } }


		/// <summary>
		/// 数据库字段 Name（System.String） 的字符串表示形式。源的注释为：计划名称
		/// </summary>
		public const String F_Name = "Name";
		/// <summary>
		/// 计划名称
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String Name
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		String ISchedulePlanning.Name
		{ get{ return this.Name; } set{ this.Name = value; } }


		/// <summary>
		/// 数据库字段 BeginTime（System.DateTime） 的字符串表示形式。源的注释为：计划启动时间
		/// </summary>
		public const String F_BeginTime = "BeginTime";
		/// <summary>
		/// 计划启动时间
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public DateTime BeginTime
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		DateTime ISchedulePlanning.BeginTime
		{ get{ return this.BeginTime; } set{ this.BeginTime = value; } }


		/// <summary>
		/// 数据库字段 JobStartTime（System.DateTime） 的字符串表示形式。源的注释为：开始工作时间
		/// </summary>
		public const String F_JobStartTime = "JobStartTime";
		/// <summary>
		/// 开始工作时间
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public DateTime JobStartTime
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		DateTime ISchedulePlanning.JobStartTime
		{ get{ return this.JobStartTime; } set{ this.JobStartTime = value; } }


		/// <summary>
		/// 数据库字段 ETA（System.Int32） 的字符串表示形式。源的注释为：执行每次的时长（以毫秒为单位）
		/// </summary>
		public const String F_ETA = "ETA";
		/// <summary>
		/// 执行每次的时长（以毫秒为单位）
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 ETA
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 ISchedulePlanning.ETA
		{ get{ return this.ETA; } set{ this.ETA = value; } }


		/// <summary>
		/// 数据库字段 JobStopTime（System.DateTime） 的字符串表示形式。源的注释为：结束工作时间
		/// </summary>
		public const String F_JobStopTime = "JobStopTime";
		/// <summary>
		/// 结束工作时间
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public DateTime JobStopTime
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		DateTime ISchedulePlanning.JobStopTime
		{ get{ return this.JobStopTime; } set{ this.JobStopTime = value; } }


		/// <summary>
		/// 数据库字段 PlanType（System.Int32） 的字符串表示形式。源的注释为：类别：0 Inactive（不活动的），1 固定时间，2 时间段 3 混合（时间段内的固定时间）
		/// </summary>
		public const String F_PlanType = "PlanType";
		/// <summary>
		/// 类别：0 Inactive（不活动的），1 固定时间，2 时间段 3 混合（时间段内的固定时间）
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 PlanType
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 ISchedulePlanning.PlanType
		{ get{ return this.PlanType; } set{ this.PlanType = value; } }


		/// <summary>
		/// 数据库字段 Loaded（System.Int32） 的字符串表示形式。源的注释为：是否加载：1已加载，0未加载
		/// </summary>
		public const String F_Loaded = "Loaded";
		/// <summary>
		/// 是否加载：1已加载，0未加载
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 Loaded
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 ISchedulePlanning.Loaded
		{ get{ return this.Loaded; } set{ this.Loaded = value; } }


		/// <summary>
		/// 数据库字段 LastFinishedTime（System.DateTime） 的字符串表示形式。源的注释为：最后完成时间
		/// </summary>
		public const String F_LastFinishedTime = "LastFinishedTime";
		/// <summary>
		/// 最后完成时间
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public DateTime LastFinishedTime
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		DateTime ISchedulePlanning.LastFinishedTime
		{ get{ return this.LastFinishedTime; } set{ this.LastFinishedTime = value; } }


		/// <summary>
		/// 数据库字段 Status（System.Int32） 的字符串表示形式。源的注释为：状态：0正常；1删除；2修改
		/// </summary>
		public const String F_Status = "Status";
		/// <summary>
		/// 状态：0正常；1删除；2修改
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 Status
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 ISchedulePlanning.Status
		{ get{ return this.Status; } set{ this.Status = value; } }


		#endregion

        #region 数据库查询及操作方法

        /// <summary>
        /// 查询是否有符合条件的数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Boolean Exists(SqlWhereBuilder where)
        {
            return TableBaseAdapter.Exists<SchedulePlanning>(where);
        }

        /// <summary>
        /// 根据条件获取表中数据总数。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Int32 GetDataCount(SqlWhereBuilder where)
        {
            return Helpers.GetDataCount<SchedulePlanning>(SchedulePlanning.GetPrimaryKeyName(), where);
        }

        /// <summary>
        /// 强制只获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static SchedulePlanning SelectSingleOnly(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.SelectSingleOnly<SchedulePlanning>(where, fields);
        }

        /// <summary>
        /// 获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='moreOrError'>用以进行确认数据重复的状态的选项；该参数输入为 true 时则数据重复时将引发异常，否则输出为数据重复的状态。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static SchedulePlanning SelectSingleOnly(SqlWhereBuilder where, ref Boolean moreOrError, params String[] fields)
        {
            return Helpers.SelectSingleOnly<SchedulePlanning>(where, ref moreOrError, fields);
        }

        /// <summary>
        /// 查询结果集形式的数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetData(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetData<SchedulePlanning>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetTop<SchedulePlanning>(top, where, fields);
        }

        /// <summary>
        /// 查询非结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<SchedulePlanning> GetDataByTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetDataByTop<SchedulePlanning>(top, where, fields);
        }

        /// <summary>
        /// 获得数据列表。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<SchedulePlanning> Select(SqlWhereBuilder where, params String[] fields)
        {
            return TableBaseAdapter.Select<SchedulePlanning>(where, true, fields, false, false, true);
        }

        /// <summary>
        /// 获得所有数据列表。
        /// </summary>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<SchedulePlanning> SelectAll(params String[] fields)
        {
            return SchedulePlanning.Select(SqlWhereBuilder.None, fields);
        }

        /// <summary>
        /// 向指定的数据库表中插入数据。
        /// </summary>
        /// <param name="fieldsWithValue">要插入的列与值。</param>
        public static Object InsertBy(KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.InsertBy<SchedulePlanning>(fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中更新匹配数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fieldsWithValue">要更新的列与值。</param>
        public static Int32 UpdateBy(SqlWhereBuilder where, KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.UpdateBy<SchedulePlanning>(where, fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中删除匹配数据。
        /// </summary>
        /// <param name="where">查询条件</param>
        public static Int32 DeleteBy(SqlWhereBuilder where)
        {
            return TableBaseAdapter.DeleteBy<SchedulePlanning>(where);
        }

        #endregion
	}
}
