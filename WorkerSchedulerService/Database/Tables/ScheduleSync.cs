using System;
using System.Data;
using System.Collections.Generic;
using MathBasic;
using MathBasic.Collections;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.Adapter;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using WorkerSchedulerService.Database;
using WorkerSchedulerService.Database.Tables;
using WorkerSchedulerService.Database.Table.Model;
using WorkerSchedulerService.Database.Table.Model.Interfaces;
using WorkerSchedulerService.Database.View.Model;
using WorkerSchedulerService.Database.View.Model.Interfaces;

namespace WorkerSchedulerService.Database.Tables
{
	internal sealed partial class ScheduleSync : TableScheme, IScheduleSync
	{
		#region 实例自身相关的操作

        static ScheduleSync()
        {
            ScheduleSync scheme = ScheduleSync.New;
            ScheduleSync.m_ClassTableName = scheme.TableName;
            ScheduleSync.m_PrimaryKeyName = Helpers.GetSpecificPrimaryKey(scheme.GetPrimaryFields());
        }

        private ScheduleSync()
        {
        }

        /// <summary>
        /// 生成此类的新实例。
        /// </summary>
        public static ScheduleSync New
        {
            get
            {
                return new ScheduleSync();
            }
        }

		#endregion

		#region 获得表映射类的相关信息

		private static String m_PrimaryKeyName;
		/// <summary>
		/// 获取此类所映射的表的某个主键名称。
		/// </summary>
		public static String GetPrimaryKeyName()
		{
			return ScheduleSync.m_PrimaryKeyName;
		}

		private static String m_ClassTableName;
		/// <summary>
		/// 获取此类所映射的表的名称。
		/// </summary>
		public static String GetClassTableName()
		{
			return ScheduleSync.m_ClassTableName;
		}

		#endregion

		#region 已映射到表的字段属性

		/// <summary>
		/// 数据库字段 ID（System.Int32） 的字符串表示形式。源的注释为：
		/// </summary>
		public const String F_ID = "ID";
		/// <summary>
		/// 
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Primary)]
		public Int32 ID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleSync.ID
		{ get{ return this.ID; } set{ this.ID = value; } }


		/// <summary>
		/// 数据库字段 PreviousPlanID（System.Int32） 的字符串表示形式。源的注释为：前一个计划ID
		/// </summary>
		public const String F_PreviousPlanID = "PreviousPlanID";
		/// <summary>
		/// 前一个计划ID
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 PreviousPlanID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleSync.PreviousPlanID
		{ get{ return this.PreviousPlanID; } set{ this.PreviousPlanID = value; } }


		/// <summary>
		/// 数据库字段 OrderPlanID（System.Int32） 的字符串表示形式。源的注释为：当前计划ID
		/// </summary>
		public const String F_OrderPlanID = "OrderPlanID";
		/// <summary>
		/// 当前计划ID
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 OrderPlanID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleSync.OrderPlanID
		{ get{ return this.OrderPlanID; } set{ this.OrderPlanID = value; } }


		/// <summary>
		/// 数据库字段 ParentID（System.Int32） 的字符串表示形式。源的注释为：
		/// </summary>
		public const String F_ParentID = "ParentID";
		/// <summary>
		/// 
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 ParentID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleSync.ParentID
		{ get{ return this.ParentID; } set{ this.ParentID = value; } }


		/// <summary>
		/// 数据库字段 ExecutionTag（System.Int32） 的字符串表示形式。源的注释为：执行标记：0未执行，1已执行
		/// </summary>
		public const String F_ExecutionTag = "ExecutionTag";
		/// <summary>
		/// 执行标记：0未执行，1已执行
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 ExecutionTag
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleSync.ExecutionTag
		{ get{ return this.ExecutionTag; } set{ this.ExecutionTag = value; } }


		#endregion

        #region 数据库查询及操作方法

        /// <summary>
        /// 查询是否有符合条件的数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Boolean Exists(SqlWhereBuilder where)
        {
            return TableBaseAdapter.Exists<ScheduleSync>(where);
        }

        /// <summary>
        /// 根据条件获取表中数据总数。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Int32 GetDataCount(SqlWhereBuilder where)
        {
            return Helpers.GetDataCount<ScheduleSync>(ScheduleSync.GetPrimaryKeyName(), where);
        }

        /// <summary>
        /// 强制只获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static ScheduleSync SelectSingleOnly(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.SelectSingleOnly<ScheduleSync>(where, fields);
        }

        /// <summary>
        /// 获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='moreOrError'>用以进行确认数据重复的状态的选项；该参数输入为 true 时则数据重复时将引发异常，否则输出为数据重复的状态。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static ScheduleSync SelectSingleOnly(SqlWhereBuilder where, ref Boolean moreOrError, params String[] fields)
        {
            return Helpers.SelectSingleOnly<ScheduleSync>(where, ref moreOrError, fields);
        }

        /// <summary>
        /// 查询结果集形式的数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetData(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetData<ScheduleSync>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetTop<ScheduleSync>(top, where, fields);
        }

        /// <summary>
        /// 查询非结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<ScheduleSync> GetDataByTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetDataByTop<ScheduleSync>(top, where, fields);
        }

        /// <summary>
        /// 获得数据列表。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<ScheduleSync> Select(SqlWhereBuilder where, params String[] fields)
        {
            return TableBaseAdapter.Select<ScheduleSync>(where, true, fields, false, false, true);
        }

        /// <summary>
        /// 获得所有数据列表。
        /// </summary>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<ScheduleSync> SelectAll(params String[] fields)
        {
            return ScheduleSync.Select(SqlWhereBuilder.None, fields);
        }

        /// <summary>
        /// 向指定的数据库表中插入数据。
        /// </summary>
        /// <param name="fieldsWithValue">要插入的列与值。</param>
        public static Object InsertBy(KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.InsertBy<ScheduleSync>(fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中更新匹配数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fieldsWithValue">要更新的列与值。</param>
        public static Int32 UpdateBy(SqlWhereBuilder where, KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.UpdateBy<ScheduleSync>(where, fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中删除匹配数据。
        /// </summary>
        /// <param name="where">查询条件</param>
        public static Int32 DeleteBy(SqlWhereBuilder where)
        {
            return TableBaseAdapter.DeleteBy<ScheduleSync>(where);
        }

        #endregion
	}
}
