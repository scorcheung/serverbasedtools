using System;
using System.Data;
using System.Collections.Generic;
using MathBasic;
using MathBasic.Collections;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.Adapter;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using WorkerSchedulerService.Database;
using WorkerSchedulerService.Database.Tables;
using WorkerSchedulerService.Database.Table.Model;
using WorkerSchedulerService.Database.Table.Model.Interfaces;
using WorkerSchedulerService.Database.View.Model;
using WorkerSchedulerService.Database.View.Model.Interfaces;

namespace WorkerSchedulerService.Database.Tables
{
	internal sealed partial class ScheduleWorker : TableScheme, IScheduleWorker
	{
		#region 实例自身相关的操作

        static ScheduleWorker()
        {
            ScheduleWorker scheme = ScheduleWorker.New;
            ScheduleWorker.m_ClassTableName = scheme.TableName;
            ScheduleWorker.m_PrimaryKeyName = Helpers.GetSpecificPrimaryKey(scheme.GetPrimaryFields());
        }

        private ScheduleWorker()
        {
        }

        /// <summary>
        /// 生成此类的新实例。
        /// </summary>
        public static ScheduleWorker New
        {
            get
            {
                return new ScheduleWorker();
            }
        }

		#endregion

		#region 获得表映射类的相关信息

		private static String m_PrimaryKeyName;
		/// <summary>
		/// 获取此类所映射的表的某个主键名称。
		/// </summary>
		public static String GetPrimaryKeyName()
		{
			return ScheduleWorker.m_PrimaryKeyName;
		}

		private static String m_ClassTableName;
		/// <summary>
		/// 获取此类所映射的表的名称。
		/// </summary>
		public static String GetClassTableName()
		{
			return ScheduleWorker.m_ClassTableName;
		}

		#endregion

		#region 已映射到表的字段属性

		/// <summary>
		/// 数据库字段 ID（System.Int32） 的字符串表示形式。源的注释为：
		/// </summary>
		public const String F_ID = "ID";
		/// <summary>
		/// 
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Primary)]
		public Int32 ID
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleWorker.ID
		{ get{ return this.ID; } set{ this.ID = value; } }


		/// <summary>
		/// 数据库字段 Name（System.String） 的字符串表示形式。源的注释为：工作名称
		/// </summary>
		public const String F_Name = "Name";
		/// <summary>
		/// 工作名称
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String Name
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		String IScheduleWorker.Name
		{ get{ return this.Name; } set{ this.Name = value; } }


		/// <summary>
		/// 数据库字段 InterfaceLink（System.String） 的字符串表示形式。源的注释为：接口引用
		/// </summary>
		public const String F_InterfaceLink = "InterfaceLink";
		/// <summary>
		/// 接口引用
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String InterfaceLink
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		String IScheduleWorker.InterfaceLink
		{ get{ return this.InterfaceLink; } set{ this.InterfaceLink = value; } }


		/// <summary>
		/// 数据库字段 LinkTimeout（System.Int32） 的字符串表示形式。源的注释为：引用请求超时时长
		/// </summary>
		public const String F_LinkTimeout = "LinkTimeout";
		/// <summary>
		/// 引用请求超时时长
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 LinkTimeout
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleWorker.LinkTimeout
		{ get{ return this.LinkTimeout; } set{ this.LinkTimeout = value; } }


		/// <summary>
		/// 数据库字段 InterfaceArgs（System.String） 的字符串表示形式。源的注释为：接口参数
		/// </summary>
		public const String F_InterfaceArgs = "InterfaceArgs";
		/// <summary>
		/// 接口参数
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public String InterfaceArgs
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		String IScheduleWorker.InterfaceArgs
		{ get{ return this.InterfaceArgs; } set{ this.InterfaceArgs = value; } }


		/// <summary>
		/// 数据库字段 Loaded（System.Int32） 的字符串表示形式。源的注释为：是否加载：1已加载，0未加载
		/// </summary>
		public const String F_Loaded = "Loaded";
		/// <summary>
		/// 是否加载：1已加载，0未加载
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 Loaded
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleWorker.Loaded
		{ get{ return this.Loaded; } set{ this.Loaded = value; } }


		/// <summary>
		/// 数据库字段 Status（System.Int32） 的字符串表示形式。源的注释为：状态：0正常；1删除
		/// </summary>
		public const String F_Status = "Status";
		/// <summary>
		/// 状态：0正常；1删除
		/// </summary>
		[DbFieldDataInfo(FieldTypes.Simple)]
		public Int32 Status
		{ get; set; }
		[DbFieldDataInfo(FieldTypes.None)]
		Int32 IScheduleWorker.Status
		{ get{ return this.Status; } set{ this.Status = value; } }


		#endregion

        #region 数据库查询及操作方法

        /// <summary>
        /// 查询是否有符合条件的数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Boolean Exists(SqlWhereBuilder where)
        {
            return TableBaseAdapter.Exists<ScheduleWorker>(where);
        }

        /// <summary>
        /// 根据条件获取表中数据总数。
        /// </summary>
        /// <param name="where">查询条件。</param>
        public static Int32 GetDataCount(SqlWhereBuilder where)
        {
            return Helpers.GetDataCount<ScheduleWorker>(ScheduleWorker.GetPrimaryKeyName(), where);
        }

        /// <summary>
        /// 强制只获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static ScheduleWorker SelectSingleOnly(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.SelectSingleOnly<ScheduleWorker>(where, fields);
        }

        /// <summary>
        /// 获得单条数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='moreOrError'>用以进行确认数据重复的状态的选项；该参数输入为 true 时则数据重复时将引发异常，否则输出为数据重复的状态。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static ScheduleWorker SelectSingleOnly(SqlWhereBuilder where, ref Boolean moreOrError, params String[] fields)
        {
            return Helpers.SelectSingleOnly<ScheduleWorker>(where, ref moreOrError, fields);
        }

        /// <summary>
        /// 查询结果集形式的数据。
        /// </summary>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetData(SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetData<ScheduleWorker>(where, fields);
        }

        /// <summary>
        /// 查询结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetTop<ScheduleWorker>(top, where, fields);
        }

        /// <summary>
        /// 查询非结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name='where'>查询条件。</param>
        /// <param name='fields'>查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<ScheduleWorker> GetDataByTop(Int32 top, SqlWhereBuilder where, params String[] fields)
        {
            return Helpers.GetDataByTop<ScheduleWorker>(top, where, fields);
        }

        /// <summary>
        /// 获得数据列表。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<ScheduleWorker> Select(SqlWhereBuilder where, params String[] fields)
        {
            return TableBaseAdapter.Select<ScheduleWorker>(where, true, fields, false, false, true);
        }

        /// <summary>
        /// 获得所有数据列表。
        /// </summary>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<ScheduleWorker> SelectAll(params String[] fields)
        {
            return ScheduleWorker.Select(SqlWhereBuilder.None, fields);
        }

        /// <summary>
        /// 向指定的数据库表中插入数据。
        /// </summary>
        /// <param name="fieldsWithValue">要插入的列与值。</param>
        public static Object InsertBy(KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.InsertBy<ScheduleWorker>(fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中更新匹配数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fieldsWithValue">要更新的列与值。</param>
        public static Int32 UpdateBy(SqlWhereBuilder where, KeyValues fieldsWithValue)
        {
            return TableBaseAdapter.UpdateBy<ScheduleWorker>(where, fieldsWithValue);
        }

        /// <summary>
        /// 从指定的数据库表中删除匹配数据。
        /// </summary>
        /// <param name="where">查询条件</param>
        public static Int32 DeleteBy(SqlWhereBuilder where)
        {
            return TableBaseAdapter.DeleteBy<ScheduleWorker>(where);
        }

        #endregion
	}
}
