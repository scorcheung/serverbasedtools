using System;
using WorkerSchedulerService.Database.Table.Model;

namespace WorkerSchedulerService.Database.View.Model
{
    /// <summary>
    /// 此类表示数据库中视图映射的最终基架构。
    /// </summary>
    internal abstract partial class ViewScheme : TableSchemeBase
    {
        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        protected ViewScheme() { }

        public override Object Insert(params String[] fields)
        {
            throw new NotSupportedException(String.Format("此方法被禁止使用，请改用 {0} 类的表实例属性进行操作。示例：{0}.Get_<ReferenceTableClassName>_Part().Insert(fields)", base.GetUnderlyingOriginalName()));
        }
        public override Object InsertWithout(params String[] fields)
        {
            throw new NotSupportedException(String.Format("此方法被禁止使用，请改用 {0} 类的表实例属性进行操作。示例：{0}.Get_<ReferenceTableClassName>_Part().InsertWithout(fields)", base.GetUnderlyingOriginalName()));
        }
        public override Int32 Update(params String[] fields)
        {
            throw new NotSupportedException(String.Format("此方法被禁止使用，请改用 {0} 类的表实例属性进行操作。示例：{0}.Get_<ReferenceTableClassName>_Part().Update(fields)", base.GetUnderlyingOriginalName()));
        }        
        public override Int32 UpdateWithout(params String[] fields)
        {
            throw new NotSupportedException(String.Format("此方法被禁止使用，请改用 {0} 类的表实例属性进行操作。示例：{0}.Get_<ReferenceTableClassName>_Part().UpdateWithout(fields)", base.GetUnderlyingOriginalName()));
        }
        public override Int32 Delete()
        {
            throw new NotSupportedException(String.Format("此方法被禁止使用，请改用 {0} 类的表实例属性进行操作。示例：{0}.Get_<ReferenceTableClassName>_Part().Delete()", base.GetUnderlyingOriginalName()));
        }

    }

}

namespace WorkerSchedulerService.Database.View.Model.Interfaces{ }
