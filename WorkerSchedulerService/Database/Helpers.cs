using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using MathBasic;
using MathBasic.Data;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.ExtendedModules.Ext1.Database.Table.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.Adapter;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;
using MathBasic.Info;
using MathBasic.Managements;
using WorkerSchedulerService.Database.Table.Model;
using WorkerSchedulerService.Database.View.Model;

namespace WorkerSchedulerService.Database
{
    /// <summary>
    /// 此类提供数据库辅助操作。
    /// </summary>
    internal static partial class Helpers
    {

        /// <summary>
        /// 获取若干主键中的某个推荐主键。
        /// </summary>
        /// <param name="primaryKeys">若干主键。</param>
        public static String GetSpecificPrimaryKey(DbField[] primaryKeys)
        {
            if (primaryKeys.IsEmpty()) {
                return "*";
            }
            else {
                foreach (DbField c in primaryKeys) {
                    if (c.IsFixedPrimaryKey) {
                        return c.Name;
                    }
                }
                return primaryKeys[0].Name;
            }
        }

        /// <summary>
        /// 复制视图数据实例的字段覆盖到某个表的数据实例上。
        /// </summary>
        /// <param name="view">视图的数据实例。</param>
        /// <param name="table">表的数据实例。</param>
        public static Int32 CopyViewFields(ViewScheme view, TableScheme table)
        {
            return DatabaseBase<DbField>.CopyElements(view, table, false, false, true);
        }

        /// <summary>
        /// 生成某个数据表的 <see cref="DataTable"/> 实例架构。
        /// </summary>
        /// <typeparam name="T">数据遍历容器的基础实例类型。</typeparam>
        /// <param name="table">表实例的引用；如果不确定此参数，请将此参数设置成“空”。</param>
        /// <param name="fields">要生成的字段列；此参数为“空”则生成全部字段列。</param>
        public static DataTable CreateDataTableScheme<T>(T table, params String[] fields) where T : TableSchemeBase
        {
            if (table == null) {
                table = DatabaseBase.CreateInstanceByT<T>();
            }
            DataTable dataTable = new DataTable();
            DataColumnCollection columns = dataTable.Columns;
            table.ValidateWithGetFields(fields, (field) =>
            {
                columns.Add(new DataColumn()
                {
                    AllowDBNull = true,
                    ColumnName = field.Name,
                    DataType = field.DataType
                });
                return MemberFoundResult.Continue;
            });
            return dataTable;
        }

        /// <summary>
        /// 生成某个数据遍历容器的 <see cref="DataTable"/> 实例。
        /// </summary>
        /// <typeparam name="T">数据遍历容器的基础实例类型。</typeparam>
        /// <param name="dataContainer">数据遍历容器的基础实例。</param>
        /// <param name="table">表实例的引用；如果不确定此参数，请将此参数设置成“空”。</param>
        /// <param name="fields">要生成的字段列；此参数为“空”则生成全部字段列。</param>
        public static DataTable CreateDataTable<T>(IEnumerable<T> dataContainer, T table, params String[] fields) where T : TableSchemeBase
        {
            if (table == null) {
                table = DatabaseBase.CreateInstanceByT<T>();
            }
            DataTable dataTable = Helpers.CreateDataTableScheme<T>(table, fields);
            dataTable.TableName = "Table";
            if (dataContainer != null) {
                DataRowCollection rows = dataTable.Rows;
                foreach (T value in dataContainer) {
                    DataRow row = dataTable.NewRow();
                    foreach (DataColumn column in dataTable.Columns) {
                        row[column] = value[column.ColumnName];
                    }
                    rows.Add(row);
                }
            }
            return dataTable;
        }

        /// <summary>
        /// 生成某个数据遍历容器的 <see cref="DataSet"/> 实例。
        /// </summary>
        /// <typeparam name="T">数据遍历容器的基础实例类型。</typeparam>
        /// <param name="dataContainer">数据遍历容器的基础实例。</param>
        /// <param name="table">表实例的引用；如果不确定此参数，请将此参数设置成“空”。</param>
        /// <param name="fields">要生成的字段列；此参数为“空”则生成全部字段列。</param>
        public static DataSet CreateDataSet<T>(IEnumerable<T> dataContainer, T table, params String[] fields) where T : TableSchemeBase
        {
            if (table == null) {
                table = DatabaseBase.CreateInstanceByT<T>();
            }
            DataTable dataTable = Helpers.CreateDataTableScheme<T>(table, fields);
            DataSet dataSet = new DataSet();
            if (dataContainer != null) {
                DataTableCollection dataTables = dataSet.Tables;
                dataTables.Add(dataTable);
                Int32 groupIndex = 1;
                DataRowCollection rows = dataTable.Rows;
                foreach (T value in dataContainer) {
                    if (groupIndex != (groupIndex = ((IDataSchema)value).GroupIdentity)) {
                        dataTable = dataTable.Clone();
                        dataTable.TableName = String.Concat("Table", groupIndex);
                        dataTables.Add(dataTable);
                    }
                    DataRow row = dataTable.NewRow();
                    foreach (DataColumn column in dataTable.Columns) {
                        row[column] = value[column.ColumnName];
                    }
                    rows.Add(row);
                }
            }
            return dataSet;
        }

        /// <summary>
        /// 根据条件获取表中数据总数。
        /// </summary>
        /// <param name="fieldToCount">查询行计数的字段部分。</param>
        /// <param name="where">查询条件。</param>
        public static Int32 GetDataCount<T>(String fieldToCount, SqlWhereBuilder where) where T : TableSchemeBase
        {
            T table = DatabaseBase.CreateInstanceByT<T>();
            if (fieldToCount != "*") {
                table.ValidateFields(new String[] { fieldToCount });
            }
            DbParameter[] parameters = null;
            String commandText = $"SELECT COUNT({fieldToCount}) FROM {table.GetTableName()}";
            if (!where.IsEmpty) {
                parameters = where.GetParameters(out String whereText);
                commandText = $"{commandText} WHERE {whereText}";
            }
            CommandScope scope = table.CommandScope;
            try {
                if (scope != null) {
                    scope.Lock();
                }
                using (DbDataReader reader = (scope != null ? scope.CreateDataReader(commandText, parameters) : table.GetDbHandler().CreateDataReader(commandText, parameters))) {
                    if (reader.Read()) {
                        Object value = reader[0];
                        if (value != DBNull.Value) {
                            return Convert.ToInt32(value);
                        }
                    }
                }
            }
            finally {
                if (scope != null && scope.IsLocked) {
                    scope.Unlock();
                }
            }
            return 0;
        }

        /// <summary>
        /// 强制只获得单条数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static T SelectSingleOnly<T>(SqlWhereBuilder where, params String[] fields) where T : TableSchemeBase
        {
            Boolean moreOrError = true;
            return Helpers.SelectSingleOnly<T>(where, ref moreOrError, fields);
        }

        /// <summary>
        /// 获得单条数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="moreOrError">用以进行确认数据重复的状态的选项；该参数输入为 true 时则数据重复时将引发异常，否则输出为数据重复的状态。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static T SelectSingleOnly<T>(SqlWhereBuilder where, ref Boolean moreOrError, params String[] fields) where T : TableSchemeBase
        {
            T value = null;
            try {
                Boolean exists = false;
                foreach (T v in TableBaseAdapter.Select<T>(where, true, fields, false, moreOrError, true)) {
                    if (!exists) {
                        exists = true;
                    }
                    else {
                        moreOrError = true;
                        break;
                    }
                    value = v;
                }
            }
            catch (Exception error) {
                if (error.Message.Contains("不只是一条数据")) {
                    throw new ApplicationException($"表 {value.GetTableName()} 基于条件 （{where.ToString()}）的查询结果不只是一条数据。", error);
                }
                else {
                    throw;
                }
            }
            return value;
        }

        /// <summary>
        /// 查询结果集形式的数据。
        /// </summary>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetData<T>(SqlWhereBuilder where, params String[] fields) where T : TableSchemeBase
        {
            T table = DatabaseBase.CreateInstanceByT<T>();
            DbParameter[] parameters = null;
            String commandText = $"SELECT {(ArrayInfo.IsNullOrEmpty(fields) ? "*" : fields.ToString(","))} FROM {table.GetTableName()}";
            if (!where.IsEmpty) {
                parameters = where.GetParameters(out String whereText);
                commandText = $"{commandText} WHERE {whereText}";
            }
            return Helpers.CreateDataTable<T>(TableBaseAdapter.Select<T>(commandText, parameters, false, true, table), table, fields);
        }

        /// <summary>
        /// 查询结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static DataTable GetTop<T>(Int32 top, SqlWhereBuilder where, params String[] fields) where T : TableSchemeBase
        {
            return Helpers.CreateDataTable<T>(Helpers.GetDataByTop<T>(top, where, fields), null, fields);
        }

        /// <summary>
        /// 查询非结果集形式的若干头条数据。
        /// </summary>
        /// <param name="top">指示最多应取数据的行数。</param>
        /// <param name="where">查询条件。</param>
        /// <param name="fields">查询字段；此参数为“空”则查询全部字段。</param>
        public static IEnumerable<T> GetDataByTop<T>(Int32 top, SqlWhereBuilder where, params String[] fields) where T : TableSchemeBase
        {
            T table = DatabaseBase.CreateInstanceByT<T>();
            Boolean fieldsEmpty = ArrayInfo.IsNullOrEmpty(fields);
            if (!fieldsEmpty) {
                table.ValidateFields(fields);
            }
            DbParameter[] parameters = null;
            String commandText = $"SELECT {(ArrayInfo.IsNullOrEmpty(fields) ? "*" : fields.ToString(","))} FROM {table.GetTableName()}";
            if (!where.IsEmpty) {
                parameters = where.GetParameters(out String whereText);
                commandText = $"{commandText} WHERE {whereText} LIMIT {top}";
            }
            return TableBaseAdapter.Select<T>(commandText, parameters, false, true, table);
        }

    }

}
