using MathBasic.ExtendedModules.Ext1.Database.StoredProcedure.Model;

namespace WorkerSchedulerService.Database.StoredProcedure.Model
{
    /// <summary>
    /// 此类表示数据库中存储过程映射的最终基架构。
    /// </summary>
    internal abstract partial class StoredProcedureScheme : StoredProcedureBase
    {
        /// <summary>
        /// 初始化此类的新实例。
        /// </summary>
        protected StoredProcedureScheme()
        {
            base.DbServerType = Common.DbServerType;
            base.DbConnectionSelection = Common.DbConnectionString;
        }

    }

}
