﻿using System;
using System.Configuration;
using System.Threading;
using AppShellSpace;
using MathBasic;
using MathBasic.Collections;
using MathBasic.Collections.Generic;
using MathBasic.Data;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.Threading;
using WorkerSchedulerService.Database.Table.Model;
using WorkerSchedulerService.Database.Tables;
using static WorkerSchedulerService.PlanningWorker;

namespace WorkerSchedulerService
{
    internal sealed class WorkerSchedulerServiceCore : WorkingPool
    {
        private volatile Int32 JobCount;

        private ScheduleWorker m_DbOwner;

        private readonly KeyValues Schedulers;
        private readonly FixedKeyWithValueCollection<PSS> PreviousPlanningWorkers;

        public WorkerSchedulerServiceCore()
            : base()
        {
            this.JobCount = 0;
            this.Schedulers = new KeyValues();
            this.PreviousPlanningWorkers = new FixedKeyWithValueCollection<PSS>();
        }

        private void UpdateProfile()
        {
            String dbServerType = null;
            if (Common.Release) {
                Configuration configuration = base.GetConfiguration(true);
                dbServerType = FileObj.ReadConfiguration(configuration, "DbServerType", ConfigType.AppSettings);
                Common.DbConnectionString = FileObj.ReadConfiguration(configuration, DbConnectionSelection.Sqlconstring.ToString(), ConfigType.ConnectionStrings);
            }
            else {
                dbServerType = FileObj.ReadAppConfig("DbServerType", ConfigType.AppSettings);
                Common.DbConnectionString = FileObj.ReadAppConfig(DbConnectionSelection.Sqlconstring.ToString(), ConfigType.ConnectionStrings);
            }
            if (dbServerType == "MySQL") {
                Common.DbServerType = DbServerType.MySQL;
            }
            else {
                Common.DbServerType = DbServerType.MSSQL;
            }
        }

        private void SearchAndUpdateScheduler(Boolean onStart)
        {
            try {
                Interlocked.Increment(ref this.JobCount);
                this.m_DbOwner.GetDbHandler().ExecTransactionScopes((commandReaderScope) =>
                {
                    try {
                        TableSchemeBase.ThreadCommandScope = commandReaderScope;
                        foreach (ScheduleWorker worker in ScheduleWorker.SelectAll()) {
                            foreach (SchedulePlanning plan in SchedulePlanning.Select(Common.SqlWhereBuilder.Append(SchedulePlanning.F_WorkerID, worker.ID))) {
                                if (worker.Status != 0) {
                                    plan.Status = worker.Status;
                                }
                                Object key = plan.ID;
                                PlanningWorker jobAlready = this.Schedulers[key] as PlanningWorker;
                                if (jobAlready != null) {
                                    if (plan.Status == 0) {
                                        continue;
                                    }
                                    while (true) {
                                        Int32 keyIndex = this.PreviousPlanningWorkers.KeyOf(plan.ID.ToString());
                                        if (keyIndex >= 0) {
                                            this.PreviousPlanningWorkers[keyIndex] = null;
                                            this.PreviousPlanningWorkers.UpdateKey(keyIndex, new Key(String.Empty));
                                        }
                                        Int32 valueIndex = this.PreviousPlanningWorkers.ValueOf(new PSS() { OrderPlanID = plan.ID });
                                        if (valueIndex >= 0) {
                                            this.PreviousPlanningWorkers[valueIndex] = null;
                                            this.PreviousPlanningWorkers.UpdateKey(valueIndex, new Key(String.Empty));
                                        }
                                        if (keyIndex < 0 && valueIndex < 0) {
                                            break;
                                        }
                                    }
                                    jobAlready.Ended |= 1;
                                    while ((jobAlready.Ended & 2) == 0) {
                                        ThreadController.Wait(TimeSpan.FromMilliseconds(1));
                                    }
                                    if (plan.Status == 1) {
                                        ScheduleSync.DeleteBy(Common.SqlWhereBuilder.Append(ScheduleSync.F_OrderPlanID, plan.ID).Or(ScheduleSync.F_PreviousPlanID, plan.ID));
                                        plan.Delete();
                                        this.Schedulers[key] = null;
                                        continue;
                                    }
                                }
                                this.Schedulers[key] = jobAlready = new PlanningWorker(plan, worker, this.PreviousPlanningWorkers);
                                Common.Run((state) =>
                                {
                                    try {
                                        Interlocked.Increment(ref this.JobCount);
                                        while (true) {
                                            if (!ThreadController.Wait(TimeSpan.FromSeconds(2), (args) =>
                                            {
                                                return (!base.OnStopping && jobAlready.Ended == 0);
                                            }).HasValue) {
                                                break;
                                            }
                                            String message = String.Empty;
                                            try {
                                                if (base.OnPausing || jobAlready.EnsureWork(out message)) {
                                                    continue;
                                                }
                                            }
                                            catch (Exception error) {
                                                message = Common.GetFullError(error);
                                            }
                                            if (Common.Release && jobAlready.Ended == 0) {
                                                base.WriteWarningLog($"任务“{jobAlready.Worker.Name}”的计划“{jobAlready.Plan.Name}”没有成功完成，详细信息：{message}");
                                            }
                                        }
                                    }
                                    finally {
                                        try {
                                            jobAlready.Plan.GetDbHandler().ExecTransactionScopes((commandScope) =>
                                            {
                                                try {
                                                    TableSchemeBase.ThreadCommandScope = commandScope;
                                                    jobAlready.Plan.Loaded = 0;
                                                    jobAlready.Plan.Update(SchedulePlanning.F_Loaded);
                                                    commandScope.Commit();
                                                }
                                                finally {
                                                    TableSchemeBase.ThreadCommandScope = null;
                                                }
                                            }, IsolationLevel.ReadCommitted);
                                        }
                                        catch (Exception error) {
                                            if (Common.Release) {
                                                base.WriteErrorLog($"任务“{jobAlready.Worker.Name}”的计划“{jobAlready.Plan.Name}”被停止的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                                            }
                                            else {
                                                throw;
                                            }
                                        }
                                        finally {
                                            jobAlready.Ended |= 2;
                                            Interlocked.Decrement(ref this.JobCount);
                                            jobAlready.Dispose();
                                        }
                                    }
                                });
                                plan.Status = 0;
                                plan.Loaded = 1;
                                plan.PlanType = (Int32)jobAlready.Plan.RealPlan;
                                plan.Update(SchedulePlanning.F_Status, SchedulePlanning.F_Loaded, SchedulePlanning.F_PlanType);
                            }
                            if (worker.Status == 1) {
                                worker.Delete();
                            }
                            else {
                                worker.Status = 0;
                                worker.Loaded = 1;
                                worker.Update(ScheduleWorker.F_Status, ScheduleWorker.F_Loaded);
                            }
                        }
                        commandReaderScope.Commit();
                    }
                    finally {
                        TableSchemeBase.ThreadCommandScope = null;
                    }
                }, IsolationLevel.ReadCommitted);
            }
            catch (Exception error) {
                if (Common.Release) {
                    base.WriteErrorLog($"计划任务加载过程处理失败，详细信息：{Common.GetFullError(error)}");
                }
                else {
                    throw;
                }
                if (!onStart) {
                    ThreadController.Wait(TimeSpan.FromMinutes(5), (args) =>
                    {
                        return (!base.OnStopping);
                    });
                }
            }
            finally {
                Interlocked.Decrement(ref this.JobCount);
            }
        }

        protected override void OnStart()
        {
            this.UpdateProfile();
            this.m_DbOwner = ScheduleWorker.New;
            this.SearchAndUpdateScheduler(true);
        }

        protected override void OnPause()
        {
            try {
                this.m_DbOwner.GetDbHandler().ExecTransactionScopes((commandScope) =>
                {
                    try {
                        TableSchemeBase.ThreadCommandScope = commandScope;
                        SchedulePlanning.UpdateBy(Common.SqlWhereBuilder.Append(SchedulePlanning.F_Loaded, 1), new KeyValues() { { SchedulePlanning.F_Loaded, 3 } });
                        ScheduleWorker.UpdateBy(Common.SqlWhereBuilder.Append(ScheduleWorker.F_Loaded, 1), new KeyValues() { { ScheduleWorker.F_Loaded, 3 } });
                        commandScope.Commit();
                    }
                    finally {
                        TableSchemeBase.ThreadCommandScope = null;
                    }
                }, IsolationLevel.ReadCommitted);
            }
            catch (Exception error) {
                if (Common.Release) {
                    base.WriteErrorLog($"计划任务被暂停的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                }
                else {
                    throw;
                }
            }
        }

        protected override void OnContinue()
        {
            this.UpdateProfile();
            try {
                this.m_DbOwner.GetDbHandler().ExecTransactionScopes((commandScope) =>
                {
                    try {
                        TableSchemeBase.ThreadCommandScope = commandScope;
                        ScheduleWorker.UpdateBy(Common.SqlWhereBuilder.Append(ScheduleWorker.F_Loaded, 3), new KeyValues() { { ScheduleWorker.F_Loaded, 1 } });
                        SchedulePlanning.UpdateBy(Common.SqlWhereBuilder.Append(SchedulePlanning.F_Loaded, 3), new KeyValues() { { SchedulePlanning.F_Loaded, 1 } });
                        commandScope.Commit();
                    }
                    finally {
                        TableSchemeBase.ThreadCommandScope = null;
                    }
                }, IsolationLevel.ReadCommitted);
            }
            catch (Exception error) {
                if (Common.Release) {
                    base.WriteErrorLog($"计划任务从暂停状态中恢复的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                }
                else {
                    throw;
                }
            }
        }

        protected override void OnStop()
        {
            while (this.JobCount > 0) {
                ThreadController.Wait(TimeSpan.FromMilliseconds(1));
            }
            try {
                this.m_DbOwner.GetDbHandler().ExecTransactionScopes((commandScope) =>
                {
                    try {
                        TableSchemeBase.ThreadCommandScope = commandScope;
                        ScheduleWorker.UpdateBy(Common.SqlWhereBuilder.Append(ScheduleWorker.F_Loaded, 1), new KeyValues() { { ScheduleWorker.F_Loaded, 0 } });
                        commandScope.Commit();
                    }
                    finally {
                        TableSchemeBase.ThreadCommandScope = null;
                    }
                }, IsolationLevel.ReadCommitted);
                this.m_DbOwner.Dispose();
                this.m_DbOwner = null;
            }
            catch (Exception error) {
                if (Common.Release) {
                    base.WriteErrorLog($"计划任务被停止的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                }
                else {
                    throw;
                }
            }
        }

        protected override Boolean DoWorkByService()
        {
            this.SearchAndUpdateScheduler(false);
            return true;
        }

    }

}
