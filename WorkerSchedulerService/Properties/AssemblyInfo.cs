﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// 有关程序集的一般信息由以下
// 控制。更改这些特性值可修改
// 与程序集关联的信息。
[assembly: AssemblyTitle("WorkerSchedulerService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("任务计划处理运行时")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyKeyFile(@"E:\EcmaKeyFile\publication_final.snk")]

// 将 ComVisible 设置为 false 会使此程序集中的类型
//对 COM 组件不可见。如果需要从 COM 访问此程序集中的类型
//请将此类型的 ComVisible 特性设置为 true。
[assembly: ComVisible(false)]

// 如果此项目向 COM 公开，则下列 GUID 用于类型库的 ID
[assembly: Guid("e0fb3f0a-6513-4b6e-ac58-10cf1ba44b98")]
[assembly: InternalsVisibleTo("ServiceUserShell, PublicKey=00240000048000009400000006020000002400005253413100040000010001008dd936d94af952ae9732b7a705f807855253eb06db42c76e7a2a52a022875799055ac52e0aa983fc315adc88425f57f6c1c7db6ca052654ff02aab7b67d3967abd7373ec6ced2464962557cb242d03b51c5b7dd0d7c7dcf89b8d45b9f4847a6e8dab8262c2b82df1b6308a40c22b2a882c337daca0fe1305ed583776170d60f2")]

// 程序集的版本信息由下列四个值组成: 
//
//      主版本
//      次版本
//      生成号
//      修订号
//
// 可以指定所有值，也可以使用以下所示的 "*" 预置版本号和修订号
//通过使用 "*"，如下所示:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.0.0.0")]
[assembly: AssemblyFileVersion("8.0.0.1")]
