﻿using System;
using System.Text;
using System.Threading;
using MathBasic.ExtendedModules.Ext1.Database.Model;
using MathBasic.ExtendedModules.Ext1.Database.Tables.QueryLanguage;

namespace WorkerSchedulerService
{
    internal static class Common
    {
        public static readonly Boolean Release;

        public static String DbConnectionString;
        public static DbServerType DbServerType;

        static Common()
        {
            Common.Release = true;//for an order to publish this service by true value.
        }

        public static SqlWhereBuilder SqlWhereBuilder
        {
            get
            {
                switch (Common.DbServerType) {
                    case DbServerType.MSSQL:
                        return SqlWhereBuilder.MSSQL;
                    case DbServerType.MySQL:
                        return SqlWhereBuilder.MySQL;
                    default:
                        return SqlWhereBuilder.None;
                }
            }
        }

        public static String GetFullError(Exception error)
        {
            StringBuilder eb = new StringBuilder();
            do {
                eb.AppendFormat("[{0}]", error.Message);
                if ((error = error.InnerException) != null) {
                    eb.Append("  --> ");
                }
            }
            while (error != null);
            return eb.ToString();
        }

        public static void Run(ParameterizedThreadStart action)
        {
            ThreadPool.QueueUserWorkItem((state) =>
            {
                action(state);
            });
            //new Thread(action) { IsBackground = true }.Start();
        }

    }

}
