﻿using System;
using System.Text;
using MathBasic;
using MathBasic.Collections;
using MathBasic.Collections.Generic;
using MathBasic.Data;
using MathBasic.Net;
using WorkerSchedulerService.Database.Table.Model;
using WorkerSchedulerService.Database.Tables;

namespace WorkerSchedulerService
{
    internal enum SchedulerPlan
    {
        Inactive = 0,
        Fixed = 1,
        Ticks = 2,
        TicksOnFixed = 3
    }

    internal sealed class PlanningWorker : IDisposable
    {
        public sealed class PSS
        {
            public Boolean Finished;
            public Int32 OrderPlanID;
            public Int32 PreviousPlanID;

            public override Boolean Equals(Object obj)
            {
                return (obj != null && this.OrderPlanID == (obj is ValueType ? (Int32)obj : ((PSS)obj).OrderPlanID));
            }

            public override Int32 GetHashCode()
            {
                return (Int32)this.OrderPlanID;
            }

        }
        private static readonly DateTime MinValue;
        static PlanningWorker()
        {
            PlanningWorker.MinValue = new DateTime(1000, 1, 1, 0, 0, 0);
        }

        public volatile Int32 Ended;

        public SchedulePlanning Plan;
        public ScheduleWorker Worker;

        private String m_Linking;
        private FixedKeyWithValueCollection<PSS> m_PreviousPlanningWorkers;

        public PlanningWorker(SchedulePlanning plan, ScheduleWorker worker, FixedKeyWithValueCollection<PSS> prw)
        {
            this.Plan = plan;
            this.Worker = worker;
            String link = worker.InterfaceLink;
            Int32 linkPrefixIndex = link.IndexOf('?');
            if (linkPrefixIndex > 0) {
                this.m_Linking = String.Concat(link.Remove(linkPrefixIndex), "?", KeyValueOptions.Insert(link.Substring(linkPrefixIndex + 1), "planningID", plan.ID.ToString(), true));
            }
            else {
                this.m_Linking = $"{link}?planningID={plan.ID.ToString()}";
            }
            this.m_PreviousPlanningWorkers = prw;
            foreach (ScheduleSync sync in ScheduleSync.Select(Common.SqlWhereBuilder.Append(ScheduleSync.F_OrderPlanID, plan.ID))) {
                if (sync.PreviousPlanID != 0 && plan.ID != sync.PreviousPlanID) {
                    this.m_PreviousPlanningWorkers[new Key(sync.PreviousPlanID.ToString())] = new PSS() { OrderPlanID = sync.OrderPlanID, Finished = (sync.ExecutionTag == 1) };
                }
            }
            this.Ended = 0;
        }

        public override Boolean Equals(Object obj)
        {
            return (obj != null && this.Plan.ID == (obj is ValueType ? (Int32)obj : (obj as PlanningWorker)?.Plan.ID));
        }

        public override Int32 GetHashCode()
        {
            return (Int32)this.Plan.ID;
        }

        private Boolean InTime
        {
            get
            {
                if (this.Ended == 0) {
                    DateTime now = DateTime.Now;
                    if (now >= this.Plan.BeginTime) {
                        Boolean scheduleByDay = (this.Plan.JobStartTime.Date == PlanningWorker.MinValue.Date && this.Plan.JobStopTime.Date == PlanningWorker.MinValue.Date);
                        switch (this.Plan.RealPlan) {
                            case SchedulerPlan.Fixed:
                                if (scheduleByDay) {
                                    return (this.Plan.LastFinishedTime.Date != now.Date && this.Plan.JobStartTime.TimeOfDay <= now.TimeOfDay && this.Plan.JobStopTime.TimeOfDay > now.TimeOfDay);
                                }
                                else {
                                    return (this.Plan.LastFinishedTime < this.Plan.JobStartTime && this.Plan.JobStartTime <= now && this.Plan.JobStopTime > now);
                                }
                            case SchedulerPlan.TicksOnFixed:
                                if (scheduleByDay) {
                                    return (this.Plan.JobStartTime.TimeOfDay <= now.TimeOfDay && (now - this.Plan.LastFinishedTime).TotalMilliseconds >= this.Plan.ETA && this.Plan.JobStopTime.TimeOfDay > now.TimeOfDay);
                                }
                                else {
                                    return (this.Plan.JobStartTime <= now && (now - this.Plan.LastFinishedTime).TotalMilliseconds >= this.Plan.ETA && this.Plan.JobStopTime > now);
                                }
                            case SchedulerPlan.Ticks:
                                if (scheduleByDay) {
                                    return (this.Plan.JobStartTime.TimeOfDay <= now.TimeOfDay && (now - this.Plan.LastFinishedTime).TotalMilliseconds >= this.Plan.ETA);
                                }
                                else {
                                    return (this.Plan.JobStartTime <= now && (now - this.Plan.LastFinishedTime).TotalMilliseconds >= this.Plan.ETA);
                                }
                        }
                    }
                }
                return false;
            }
        }

        public Boolean EnsureWork(out String message)
        {
            String innerMessage = message = String.Empty;
            Boolean roundFinished = true;
            if (this.InTime) {
                this.Plan.GetDbHandler().ExecTransactionScopes((commandScope) =>
                {
                    try {
                        TableSchemeBase.ThreadCommandScope = commandScope;
                        foreach (PSS pss in this.m_PreviousPlanningWorkers.SelectItemsByValue(new PSS() { OrderPlanID = this.Plan.ID })) {
                            if (pss != null) {
                                if (pss.Finished) {
                                    ScheduleSync.UpdateBy(Common.SqlWhereBuilder.Append(ScheduleSync.F_PreviousPlanID, pss.PreviousPlanID).And(ScheduleSync.F_OrderPlanID, pss.OrderPlanID), new KeyValues() { { ScheduleSync.F_ExecutionTag, 0 } });
                                    pss.Finished = false;
                                }
                                else {
                                    return;
                                }
                            }
                        }
                        if (this.Ended == 0) {
                            String msg = null;
                            try {
                                RequestStatus status = WebClient.SendRequestByPOST(new Uri(this.m_Linking), this.Worker.InterfaceArgs, (Int32)this.Worker.LinkTimeout, Encoding.UTF8, out msg);
                                if (status == RequestStatus.Success && msg.Trim() == "0") {
                                    foreach (PSS pss in this.m_PreviousPlanningWorkers.SelectItemsByName(this.Plan.ID.ToString())) {
                                        if (pss != null && !pss.Finished) {
                                            pss.Finished = true;
                                        }
                                    }
                                    ScheduleSync.UpdateBy(Common.SqlWhereBuilder.Append(ScheduleSync.F_PreviousPlanID, this.Plan.ID).And(ScheduleSync.F_ExecutionTag, 0), new KeyValues() { { ScheduleSync.F_ExecutionTag, 1 } });
                                    return;
                                }
                                else {
                                    msg = $"[{status.ToString()}]->[{msg}]";
                                }
                            }
                            finally {
                                innerMessage = msg;
                                this.Plan.LastFinishedTime = DateTime.Now;
                                this.Plan.Update(SchedulePlanning.F_LastFinishedTime);
                            }
                        }
                        roundFinished = false;
                        return;
                    }
                    finally {
                        commandScope.Commit();
                        TableSchemeBase.ThreadCommandScope = null;
                    }
                }, IsolationLevel.ReadUncommitted);
                message = innerMessage;
            }
            return roundFinished;
        }

        public void Dispose()
        {
            this.m_Linking = null;
            this.m_PreviousPlanningWorkers = null;
            this.Plan.Dispose();
            this.Plan = null;
            this.Worker.Dispose();
            this.Worker = null;
        }
    }

}
