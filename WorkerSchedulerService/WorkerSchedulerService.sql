USE [WorkerScheduler]
GO
/****** Object:  Table [dbo].[SchedulePlanning]    Script Date: 2021/11/3 12:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchedulePlanning](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[WorkerID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[BeginTime] [datetime] NOT NULL,
	[JobStartTime] [datetime] NOT NULL,
	[ETA] [int] NOT NULL,
	[JobStopTime] [datetime] NOT NULL,
	[PlanType] [int] NOT NULL,
	[Loaded] [int] NOT NULL,
	[LastFinishedTime] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_SchedulePlanning] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ScheduleSync]    Script Date: 2021/11/3 12:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleSync](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PreviousPlanID] [int] NOT NULL,
	[OrderPlanID] [int] NOT NULL,
	[ParentID] [int] NOT NULL,
	[ExecutionTag] [int] NOT NULL,
 CONSTRAINT [PK_ScheduleSync] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ScheduleWorker]    Script Date: 2021/11/3 12:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ScheduleWorker](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[InterfaceLink] [nvarchar](200) NOT NULL,
	[LinkTimeout] [int] NOT NULL,
	[InterfaceArgs] [nvarchar](200) NOT NULL,
	[Loaded] [int] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_ScheduleWorker] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ScheduleSync] ADD  CONSTRAINT [DF_ScheduleSync_ParentID]  DEFAULT ((0)) FOR [ParentID]
GO
ALTER TABLE [dbo].[ScheduleSync] ADD  CONSTRAINT [DF_ScheduleSync_ExecutionTag]  DEFAULT ((0)) FOR [ExecutionTag]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工作表ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'WorkerID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计划名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计划启动时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'BeginTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开始工作时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'JobStartTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行每次的时长（以毫秒为单位）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'ETA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'结束工作时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'JobStopTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'类别：0 Inactive（不活动的），1 固定时间，2 时间段 3 混合（时间段内的固定时间）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'PlanType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否加载：1已加载，0未加载' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'Loaded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后完成时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'LastFinishedTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态：0正常；1删除；2修改' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SchedulePlanning', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'前一个计划ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleSync', @level2type=N'COLUMN',@level2name=N'PreviousPlanID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前计划ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleSync', @level2type=N'COLUMN',@level2name=N'OrderPlanID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行标记：0未执行，1已执行' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleSync', @level2type=N'COLUMN',@level2name=N'ExecutionTag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'调度表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleSync'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工作名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接口引用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker', @level2type=N'COLUMN',@level2name=N'InterfaceLink'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'引用请求超时时长' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker', @level2type=N'COLUMN',@level2name=N'LinkTimeout'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'接口参数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker', @level2type=N'COLUMN',@level2name=N'InterfaceArgs'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否加载：1已加载，0未加载' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker', @level2type=N'COLUMN',@level2name=N'Loaded'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态：0正常；1删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计划工作表' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScheduleWorker'
GO
