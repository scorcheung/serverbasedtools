﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Text;
using AppShellSpace;
using MathBasic;

namespace RunByAService
{
    internal sealed class RunByAServiceCore : WorkingPool
    {

        private static Object Locker;

        static RunByAServiceCore()
        {
            RunByAServiceCore.Locker = new Object();
        }

        public RunByAServiceCore()
            : base()
        {
            this.m_Handler = null;
            this.m_Planned = false;
            this.m_LogPath = this.m_ProcessName =
                this.m_ProcessOnRunningCommandLine = this.m_ProcessOnStoppingCommandLine = null;
        }

        private Process m_Handler;
        private StreamWriter m_FileStream;

        private String m_LogPath;
        private String m_ProcessName;

        private Boolean m_Planned;
        private Boolean m_HandlePausingEvents;

        private String m_ProcessOnRunningCommandLine;
        private String m_ProcessOnStoppingCommandLine;

        private void OutputDataReceived(Object sender, DataReceivedEventArgs e)
        {
            lock (RunByAServiceCore.Locker) {
                if (this.m_FileStream != null) {
                    String data = e.Data;
                    if (data != null && data.Trim().Length > 0) {
                        this.m_FileStream.Write($"{this.m_ProcessName}-({((Process)sender).Id})->{data}");
                    }
                    this.m_FileStream.WriteLine();
                    this.m_FileStream.Flush();
                }
            }
        }

        private Process CreateProcess(String commandLine)
        {
            Process proc = new Process();
            proc.StartInfo.FileName = "cmd.exe";
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.Arguments = String.Concat("/c ", commandLine);
            Encoding textEncoding = Encoding.UTF8;
            proc.StartInfo.StandardOutputEncoding = textEncoding;
            proc.StartInfo.RedirectStandardOutput = true;
            if (!String.IsNullOrEmpty(this.m_LogPath)) {
                if (this.m_FileStream == null) {
                    this.m_FileStream = new StreamWriter(this.m_LogPath, true, textEncoding);
                }
                proc.OutputDataReceived += this.OutputDataReceived;
            }
            return proc;
        }

        private void InitInternal()
        {
            this.m_Handler = this.CreateProcess(this.m_ProcessOnRunningCommandLine);
        }

        private void KillProcess(Process proc)
        {
            if (proc != null) {
                using (proc) {
                    if (!proc.HasExited) {
                        try {
                            proc.Kill();
                        }
                        catch { }
                    }
                }
            }
        }

        private void StopInternal()
        {
            try {
                if (!String.IsNullOrEmpty(this.m_ProcessOnStoppingCommandLine)) {
                    Process proc = this.CreateProcess(this.m_ProcessOnStoppingCommandLine);
                    proc.Start();
                    proc.BeginOutputReadLine();
                    proc.WaitForExit(30000);
                    this.KillProcess(proc);
                }
                this.KillProcess(this.m_Handler);
                this.m_Handler = null;
            }
            finally {
                lock (RunByAServiceCore.Locker) {
                    this.m_FileStream.Dispose();
                    this.m_FileStream = null;
                }
            }
        }

        protected override void OnStart()
        {
            try {
                Configuration configuration = base.GetConfiguration(true);
                this.m_LogPath = FileObj.ReadConfiguration(configuration, "LogPath", ConfigType.AppSettings);
                this.m_ProcessName = FileObj.ReadConfiguration(configuration, "ProcessName", ConfigType.AppSettings);
                Boolean.TryParse(FileObj.ReadConfiguration(configuration, "HandlePausingEvents", ConfigType.AppSettings), out this.m_HandlePausingEvents);
                this.m_ProcessOnRunningCommandLine = FileObj.ReadConfiguration(configuration, "ProcessOnRunningCommandLine", ConfigType.AppSettings);
                this.m_ProcessOnStoppingCommandLine = FileObj.ReadConfiguration(configuration, "ProcessOnStoppingCommandLine", ConfigType.AppSettings);
                this.InitInternal();
                this.m_Planned = true;
            }
            catch (Exception error) {
                if (Common.Release) {
                    base.WriteErrorLog($"进程 {this.m_ProcessName} 加载过程处理失败，详细信息：{Common.GetFullError(error)}");
                }
                else {
                    throw;
                }
            }
        }

        protected override void OnPause()
        {
            if (this.m_HandlePausingEvents) {
                try {
                    this.StopInternal();
                }
                catch (Exception error) {
                    if (Common.Release) {
                        base.WriteErrorLog($"进程 {this.m_ProcessName} 被暂停的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                    }
                    else {
                        throw;
                    }
                }
            }
        }

        protected override void OnContinue()
        {
            if (this.m_HandlePausingEvents) {
                try {
                    this.InitInternal();
                }
                catch (Exception error) {
                    if (Common.Release) {
                        base.WriteErrorLog($"进程 {this.m_ProcessName} 从暂停状态中恢复的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                    }
                    else {
                        throw;
                    }
                }
            }
        }

        protected override void OnStop()
        {
            try {
                this.StopInternal();
            }
            catch (Exception error) {
                if (Common.Release) {
                    base.WriteErrorLog($"进程 {this.m_ProcessName} 被停止的后续工作没有完成，详细信息：{Common.GetFullError(error)}");
                }
                else {
                    throw;
                }
            }
        }

        protected override Boolean DoWorkByService()
        {
            if (this.m_Planned) {
                this.m_Handler.Start();
                this.m_Handler.BeginOutputReadLine();
                this.m_Planned = false;
            }
            while (!this.m_Handler.WaitForExit(500)) {
                if (base.OnPausing || base.OnStopping) {
                    break;
                }
            }
            return true;
        }

    }

}
