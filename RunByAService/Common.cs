﻿using System;
using System.Text;
using System.Threading;
using MathBasic.ExtendedModules.Ext1.Database.Model;

namespace RunByAService
{
    internal static class Common
    {
        public static readonly Boolean Release;

        public static String DbConnectionString;
        public static DbServerType DbServerType;

        static Common()
        {
            Common.Release = true;//for an order to publish this service by true value.
        }

        public static String GetFullError(Exception error)
        {
            StringBuilder eb = new StringBuilder();
            do {
                eb.AppendFormat("[{0}]", error.Message);
                if ((error = error.InnerException) != null) {
                    eb.Append("  --> ");
                }
            }
            while (error != null);
            return eb.ToString();
        }

        public static void Run(ParameterizedThreadStart action)
        {
            ThreadPool.QueueUserWorkItem((state) =>
            {
                action(state);
            });
            //new Thread(action) { IsBackground = true }.Start();
        }

    }

}
